package coffee.source.android.unicodeMap;

import java.util.List;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class UnicodeArrayAdapter
    extends ArrayAdapter<UnicodeDisplayable> {

    protected Context context;
    protected LayoutInflater inflater;

    public UnicodeArrayAdapter(Context context) {
	super(context, 0);
	this.context = context;
	this.inflater
	    = (LayoutInflater)context
	    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public UnicodeArrayAdapter(Context context,
			       List<UnicodeDisplayable> objects) {
	super(context, 0, objects);
	this.context = context;
	this.inflater
	    = (LayoutInflater)context
	    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
	UnicodeDisplayable item = this.getItem(position);
	View view = null;
	if ( item instanceof UnicodeCharacter ) {
	    View charTextView = null;  View labelTextView = null;
	    if ( convertView != null ) {
		charTextView = convertView.findViewById(R.id.charText);
		labelTextView = convertView.findViewById(R.id.labelText);
		if ( charTextView != null && labelTextView != null )
		    view = convertView;
	    }
	    if ( view == null ) {
		view = inflater.inflate(R.layout.char_item, parent, false);
		charTextView = view.findViewById(R.id.charText);
		labelTextView = view.findViewById(R.id.labelText);
	    }
	    if ( ((UnicodeCharacter)item).isPrintable() )
		((TextView)charTextView).setText(((UnicodeCharacter)item).getChar());
	    else
		((TextView)charTextView).setText("");
	    ((TextView)labelTextView).setText(((UnicodeCharacter)item).getLabel());
	} else if ( item instanceof UnicodeRangeable ) {
	    if ( convertView != null
		 && convertView instanceof TextView ) {
		view = convertView;
	    }
	    if ( view == null )
		view = inflater.inflate(R.layout.range_item, parent, false);
	    ((TextView)view).setText(((UnicodeRangeable)item).getDescr());
	} else
	    throw new AssertionError("unknown UnicodeDisplayable");
	return view;
    }

}
