package coffee.source.android.unicodeMap;

public interface UnicodeRangeable extends UnicodeDisplayable {
    public int getFrom();
    public int getTo();
    public String getDescr();
    public String getTitle();
}
