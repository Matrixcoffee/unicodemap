#!/bin/sh

awk '
/^#/ { next }
/^[ \t]*$/ { next }
{
	rng = $0
	sub(";.*$", "", rng)
	#print "rng: >" rng "<"

	rngl = rng
	sub("\\.\\..*$", "", rngl)
	#print "rngl: >" rngl "<"

	rngh = rng
	sub("^.*\\.\\.", "", rngh)
	#print "rngh: >" rngh "<"

	txt = $0
	sub("^.*;[ \t]*", "", txt)
	#print "txt: >" txt "<"

	uslug = toupper(txt)
	gsub("[^A-Z0-9]", "_", uslug)
	gsub("__+", "_", uslug)
	#print "uslug: >" uslug "<"

	print "\t" uslug "(0x" rngl ", 0x" rngh ", \"" txt "\"),"
}

END {
	ua = "0x" rngh
	ua = sprintf("%x", ua + 1)
	print "\tUNASSIGNED(0x" ua ", 0x" ua ", \"Not Assigned\");"
}
' < UCD/Blocks.txt
