#!/bin/sh

awk '
{
	of = sprintf("assets/UnicodeData-%02d.txt", f)
	print >> of
}

++n >= 1000 {
	n=0
	f++
}' < UCD/UnicodeData.txt
